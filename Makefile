SOURCES = morphology.txt morphology_examples.txt git.txt

HTML = $(SOURCES:.txt=.html)

all: $(SOURCES) $(HTML) 
	echo "Done!"

clean:
	rm -f $(HTML) 

%.html : %.txt
	pandoc -f markdown $< -o $@
